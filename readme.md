# Curso de MongoDB

## Lecciones:

* [Lección #1 - Introducción](#lesson01)
* [Lección #2 - Instalación](#lesson02)
* [Lección #3 - Insert](#lesson03)
* [Lección #4 - Where](#lesson04)
* [Lección #5 - Where Not](#lesson05)
* [Lección #6 - Update parte 1](#lesson06)
* [Lección #7 - Update parte 2](#lesson07)
* [Lección #8 - Update parte 3](#lesson08)
* [Lección #9 - Delete](#lesson09)
* [Lección #10 - Between](#lesson10)
* [Lección #11 - Cursores](#lesson11)
* [Lección #12 - Métodos de Cursores](#lesson12)
* [Lección #13 - Arreglos](#lesson13)
* [Lección #14 - Delete arreglos](#lesson14)
* [Lección #15 - Select arreglos](#lesson15)
* [Lección #16 - Group](#lesson16)
* [Lección #17 - Expresiones Regulares](#lesson17)
* [Lección #18 - Documentos Embebidos](#lesson18)
* [Lección #19 - CRUD Documentos Embebidos](#lesson19)
* [Lección #20 - Relación uno a muchos](#lesson20)
* [Lección #21 - ElemMatch](#lesson21)
* [Lección #22 - Update SubDocumentos](#lesson22)
* [Lección #23 - ObjectId](#lesson23)
* [Lección #24 - Create Collection](#lesson24)
* [Lección #25 - Índices](#lesson25)
* [Lección #26 - Respaldo Base Datos (final)](#lesson26)

## <a name="lesson01"></a> Lección #1 - Introducción
- - - 
Para este curso se necesita un conocimiento básico de bases de datos relacionales y Javascript.

MongoDB es un motor de bases de datos NoSQL (Es decir, no vamos a usar SQL) de código abierto, escrito en C++. Se encuentra orientado al almacenamiento de documentos.

Es posible utilizarlo en Windows así como algunos sistemas UNIX.

### Características
* Se almacenan documentos en colecciones.
* Fácil cambio de schema en producción.
* Fácil escabilidad de forma horizontal.
* Replicación de datos.
* Indexación.
* Almacenamiento de archivos.
* Es posible ejecutar JS en el servidor.

### ¿Quiénes lo utilizan?
* The Guardian.
* Cisco.
* MTV.
* The New York Times.
* Forbes.
* EA.
* Nokia.
* Ebay.
* Expedia EA.
* Facebook.
* Google.
* LinkedIn.
* Etc.

### ¿Qué se verá en el curso?
* Creación de documentos.
* Funcionamiento de MongoDB.
* CRUD documentos.
* Sentencias comunes.
* Embeber documentos.
* Arreglos.
* Cursores.
* Indices.
* Integración con un lenguaje de programación.

## <a name="lesson02"></a> Lección #2 - Instalación
- - - 
Para ello debemos acceder al link y seguir cada uno de los pasos:

https://docs.mongodb.com/manual/installation/

## <a name="lesson03"></a> Lección #3 - Insert
- - - 
Para iniciar el servidor de mongo en dado caso que esté apagado, lo hacemos con el comando: 

```sh
$ mongod
```

Para entrar en la terminal de mongo, podemos hacerlo ejecutando el comando:

```sh
$ mongo
```

_Si ya establecimos el path en las variables de entorno._

Una vez dentro, podemos crear un documento (puede ser un diccionario, un hash, etc). Como es un servidor de JS lo haremos con la notación JSON de la siguiente manera:

```sh
> documento = { nombre: "Eduardo" }
```

Si escribimos:

```sh
> documento
```

Podemos observar la estructura JSON que planteamos. Los documentos pueden cambiar... Si escribimos:

```sh
> documento = { String = "Eduardo",
```

Si le damos a enter nos fijaremos que saldrán 3 puntos suspensivos

```sh
> ...
```

Aquí podremos seguir escribiendo, ejemplo:

```sh
> documento = { String = "Eduardo",
> ... numero : 27,
> ... real : 27.3
> ... flotante : true,
> ... fecha : new Date()
> ... }
```

P.D Todo lo que salga después de los 3 puntos suspensivos, debemos escribirlo nosotros. Si fue exitoso, veremos el objeto JSON planteado de la siguiente manera:

```sh
> {
>       "String" : "Eduardo",
>       "numero" : 27,
>       "real" : 27.3,
>       "flotante" : true,
>       "fecha" : ISODate("2020-04-09T14:57:36.696Z")
> }
```

Como sabemos que esto es JavaScript, podemos guardarlo de la siguiente manera:

```sh
> var documento = { String : "Eduardo", numero : 27, real: 27.3, flotante: true, fecha: new Date() }
```

Crearemos otra variable de la siguiente manera:

```sh
> otro = { nombre: "Eduardo", pais: "Venezuela" }
```

Una buena práctica es definir un estándar para definir el nombre de las propiedades. También, cuando definimos un objeto sin comillas dobles, este lo coloca de manera automática. Ejemplo, el resultado de otro es:

```sh
> otro
> { "nombre" : "Eduardo", "pais" : "Venezuela" }
```

### ¿Cómo podemos ver las bases de datos siendo yo el cliente?

Para ello debemos teclear el comando:

```sh
> show databases;
```
| BDD | Size |
| :---: | :---: |
| admin | 0.000GB |
| config | 0.000GB |
| local | 0.000GB |

Vamos a cambiarnos de base de datos, de la siguiente manera:

```sh
> use codigo_facilito;
```

Sin embargo, esta base de datos no existe... Pero no importa, ya veremos más adelante como crearla y guardar los datos allí. Para saber en que base de datos estamos tecleamos: 

```sh
> db
```

Crearemos un objeto que sea el siguiente:

```sh
> usuario = {nombre: "Test", edad: 22}
```

Para guardarlo, realizamos lo siguiente:

```sh
> db.usuarios.insert(usuario)
```

¿Qué significa este comando? Significa que nuestra base de datos actual, va a haber una colección llamada usuarios (sino existe, se creará) y almacenaremos nuestro usuario. Si aparece el mensaje WriteResult({ "nInserted" : 1 }) es porque fue creado de forma satisfactoria.

_Nota: Una colección es una serie de documentos..._

Si nos fijamos y al teclear show databases; podemos ver la base de datos codigo_facilito ya creada. Esto ocurre al igual que las colecciones, si no fue creada, mongodb lo creará de forma automática.

_Nota: Al cambiarnos a una base de datos que no existe, esta será usada mediante la memoria RAM hasta que hagamos por lo menos un insert._

Para ver los documentos creados podemos escribir:

```sh
> db.usuarios.find()
```

Esto es el equivalente a escribir un SELECT en una base de datos relacional.

Ahora guadaremos a otro usuario de la siguiente manera:

```sh
> otro = {nombre: "Otro"}
> db.usuarios.insert(otro)
```

Y si queremos verlo escribimos db.usuarios.find().

## <a name="lesson04"></a> Lección #4 - Where
- - - 
Con el comando: 

```sh
> db.usuarios.find()
```

Obtenemos todos lo objetos de la colección.

Con el comando:

```sh
> db.usuarios.findOne()
```

Obtenemos solo un objeto de la colección (El primero).

También podemos crear atajos. Por ejemplo:

```sh
> var test = db.usuarios.findOne()
```

Y si escribimos test, aparece el primer usuario.

Para hacer condicionales, simplemente usamos el método find pero le pasamos los valores que queremos buscar mediante un objeto JSON. Ejemplo:

```sh
> var test_dos = db.usuarios.find({ nombre: "Otro" })
```

Otro ejemplo:

```sh
> var test_dos = db.usuarios.find({ edad: 22 })
```

Si queremos hacer un where con dos condiciones, es de la siguiente forma:

```sh
> db.usuarios.find({ nombre: "Test", edad: 22 })
```

## <a name="lesson05"></a> Lección #5 - Where Not
- - -
Para hacer un where not, se debe realizar de la siguiente manera:

```sh
> db.usuarios.find({ edad: { $ne: 22 } })
```

\$ne significa not equals

Esto lo podemos unir con lo que ya sabemos, ejemplo:

```sh
> db.usuarios.find({ edad: 22, nombre: { $ne: "Otro"} })
```

## <a name="lesson06"></a> Lección #6 - Update parte 1
- - -
Crearemos dos documentos de la siguiente forma:

```sh
> uno = { nombre: "Test uno" }
> dos = { nombre: "Test dos" }
```

Para hacer un insert masivo y no solo 1 por 1 lo hacemos se la siguiente manera:

```sh
> db.usuarios.insert([uno, dos])
```

Debe aparecer el siguiente mensaje:

```sh
BulkWriteResult({
        "writeErrors" : [ ],
        "writeConcernErrors" : [ ],
        "nInserted" : 2,
        "nUpserted" : 0,
        "nMatched" : 0,
        "nModified" : 0,
        "nRemoved" : 0,
        "upserted" : [ ]
})
```

Ahora vamos al Test uno de la siguiente manera:

```sh
> var test = db.usuarios.findOne({ "_id" : ObjectId("5e8fd72c52c59646ca270c08") })
```

Si escribimos test, debe aparecer sus valores correspondientes. Si solo queremos ver una propiedad en específico, escribimos:

```sh
> test.nombre
```

Ahora vamos a cambiarle el nombre de la propiedad y vamos a almacenarlo en la base de datos. Para ello ejecutamos:

```sh
> test.nombre = "Cambio de nombre"
```

Pero si ejecutamos el comando:

```sh
> db.usuarios.find()
```

Podemos fijarnos que aún no está almacenado en la base de datos. Para hacer los cambios correspondientes debe ser con el comando save. Ejemplo:

```sh
> db.usuarios.save( test )
```

Si fue exitoso, debe mostrar el mensaje

```sh
> WriteResult({ "nMatched" : 1, "nUpserted" : 0, "nModified" : 1 })
```

De igual forma podemos comprobarlo ejecutando el comando db.usuarios.find()

```sh
{ "_id" : ObjectId("5e8f552e615978e0559adca0"), "nombre" : "Test", "edad" : 22 }
{ "_id" : ObjectId("5e8f572a615978e0559adca1"), "nombre" : "Otro" }
{ "_id" : ObjectId("5e8fd72c52c59646ca270c08"), "nombre" : "Cambio de nombre" }
{ "_id" : ObjectId("5e8fd72c52c59646ca270c09"), "nombre" : "Test dos" }
```

Ahí podemos ver que ya no se llama "Test uno" sino "Cambio de nombre".

### ¿Cómo funciona save?
Cuando tenemos un documento, revisa si tiene un "_id", si la comparación es exitosa, procede a actualizar un documento. Caso contrario, lo que hace es crear el documento. Hagamos otro ejemplo:

```sh
> save_prueba = { nombre: "Esto es una prueba de save" }
> db.usuarios.save(save_prueba)
```

Fíjate que el mensaje es diferente. Dice ahora lo siguiente:

```sh
> WriteResult({ "nInserted" : 1 })
```

Como podemos observa, como ese id no lo tiene (o no existe), este procede a crearlo.

## <a name="lesson07"></a> Lección #7 - Update parte 2
- - -
Ahora haremos uso del método update. Para ello, podemos ver nuestros registros con el comando:

```sh
> db.usuarios.find()
```

Y escogeremos al usuario "Test uno" con el comando:

```sh
> test = db.usuarios.findOne({ "_id" : ObjectId("5e8fd72c52c59646ca270c08") })
```

Cambiamos el nombre de la siguiente manera:

```sh
> test.nombre = "Podemos actualizar el nombre"
```

Y para agregar otra propiedad, simplemente hacemos esto:

```sh
> test.edad = 27
```

Ahora para poder actualizar nuestro registro en la base de dedatos ejecutamos:

```sh
> db.usuarios.update( { "_id" : ObjectId("5e8fd72c52c59646ca270c08") }, test )
```

El método update recibe dos parámetros. El primero es el parámetro para hacer una búsqueda con el where (se recomienda usar el id ya que es único) y el segundo es el objeto con el que lo vamos a reemplazar. Si fue exitoso el proceso, se debe mostrar el siguiente mensaje:

```sh
WriteResult({ "nMatched" : 1, "nUpserted" : 0, "nModified" : 1 })
```

Podemos comprobar nuestros cambios ejecutando:

```sh
> db.usuarios.find()
{ "_id" : ObjectId("5e8f552e615978e0559adca0"), "nombre" : "Test", "edad" : 22 }
{ "_id" : ObjectId("5e8f572a615978e0559adca1"), "nombre" : "Otro" }
{ "_id" : ObjectId("5e8fd72c52c59646ca270c08"), "nombre" : "Podemos actualizar el nombre" }
{ "_id" : ObjectId("5e8fd72c52c59646ca270c09"), "nombre" : "Test dos" }
{ "_id" : ObjectId("5e8fda1a52c59646ca270c0a"), "nombre" : "Esto es una prueba de save" }
```

## <a name="lesson08"></a> Lección #8 - Update parte 3
- - -
En esta lección vamos a seguir usando el método update. Para esta prueba, debemos crear 3 usuarios con el mismo nombre y la misma edad de manera previa:

```sh
> uno = { nombre: "Test", edad: 25 }
> dos = { nombre: "Test", edad: 25 }
> tres = { nombre: "Test", edad: 25 }
> db.usuarios.insert([uno, dos, tres])
```

Y haremos uso del siguiente método:

```sh
> db.usuarios.update({ nombre: "Test" }, { $set: { nombre: "CF" } })
```

Pero si lo ejecutamos, podemos fijarnos que solo modificará un registro. Esto ya es un comportamiento por defecto de MongoDB.

Para actualizarlos de forma másiva, debemos pasarle un parámetro adicional que es el siguiente:

```sh
> db.usuarios.update({ nombre: "Test" }, { $set: { nombre: "CF" } }, { multi: true } )
```

Con multi, le estamos diciendo a MongoDB que actualice de forma masiva. Si ejecutamos db.usuarios.find() podemos ver que todos los registros de nombre "Test" fueron cambiados a "CF". También podemos hacer cosas de este estilo:

```sh
> db.usuarios.update({ nombre: "CF" }, { $set: { edad: 28, nombre: "CF2", example: 56 } }, { multi: true } )
```

\$set no solo permite modificar valores, sino también cambiar propiedades.

Si no le ponemos parámetros de búsqueda al método update, este modificará o agregará dicho valor a todos los registos. Ejemplo:

```sh
> db.usuarios.update({}, { $set: { example: 56 } }, { multi: true } )
```

Y para eliminar la propiedad de los registros podemos hacer uso de la variable \$unset. Ejemplo:

```sh
> db.usuarios.update({}, { $unset: { example: 1 } }, { multi: true } )
```

_Nota: Debe pasarse el valor 1_

## <a name="lesson09"></a> Lección #9 - Delete
- - -
En esta lección veremos el método remove. Si deseamos eliminar recurso de la colección lo hacemos con el comando:

```sh
> db.usuarios.remove({ nombre: "Otro"})
WriteResult({ "nRemoved" : 1 })
```

Para eliminar de forma masiva lo hacemos de la siguiente forma:

```sh
> db.usuarios.remove({ nombre: "CF2"})
WriteResult({ "nRemoved" : 4 })
```

_Nota: A diferencia del update, aquí si borra de forma masiva_

Si queremos vaciar toda la colección, lo hacemos de la siguiente manera:

```sh
> db.usuarios.remove({})
WriteResult({ "nRemoved" : 3 })
```

Para eliminar una colección entera, lo hacemos con el comando:

```sh
> db.usuarios.drop()
true
```

Si ejecutamos:

```sh
> show collections
```

Veremos que no tendremos nada.

Para eliminar una base de datos, lo hacemos de la siguiente manera:

```sh
> db.dropDatabase();
{ "dropped" : "codigo_facilito", "ok" : 1 }
```

__IMPORTANTE: Esto jamás debemos hacerlo en producción__

Si ejecutamos:

```sh
> show databases;
```

| BDD | Size |
| :---: | :---: |
| admin | 0.000GB |
| config | 0.000GB |
| local | 0.000GB |

Veremos que lo tendremos como nuevo.

## <a name="lesson10"></a> Lección #10 - Between
- - -
En esta lección haremos uso del find pero buscando por rangos (mayor, menor, entre, etc.). Lo primero que necesitamos es una base de datos con una data de prueba y para ello haremos lo siguiente:

```sh
> use codigo_facilito;
> uno = {nombre: "El Hobbit", autor: "J.R.R Tolkien", numero: 100, unidad: "millones", idioma: "ingles", primera_edicion: 1937}
> dos = {nombre: "El señor de los anillos", autor: "J.R.R Tolkien", numero: 150, unidad: "millones", idioma: "ingles", primera_edicion: 1954}
> tres = {nombre: "El principito", autor: "Antonie de Saint-Exepéry", numero: 140, unidad: "millones", idioma: "frances", primera_edicion: 1943}
> cuatro = {nombre: "Historias de dos ciudades", autor: "Charles Dicken", numero: 200, unidad: "millones", idioma: "ingles", primera_edicion: 1859}
> cinco = {nombre: "Sueño en el pabellón rojo", autor: "Cao Xuequin", numero: 100, unidad: "millones", idioma: "chino", primera_edicion: 1759}
> seis = {nombre: "Triple representatividad", autor: "Jilian Zemin", numero: 100, unidad: "millones", idioma: "chino", primera_edicion: 2001}
> siete = {nombre: "Diez Negritos", autor: "Agata Christie", numero: 99, unidad: "millones", idioma: "ingles", primera_edicion: 1939}
> ocho = {nombre: "El código Da Vinci", autor: "Dan Brown", numero: 80, unidad: "millones", idioma: "ingles", primera_edicion: 2003}
> nueve = {nombre: "El nombre de la rosa", autor: "Umberto Eco", numero: 50, unidad: "millones", idioma: "ingles", primera_edicion: 1980}
> db.libros.insert([uno, dos, tres, cuatro, cinco, seis, siete, ocho, nueve])
```

A medida que vayamos trabajando en un proyecto podemos fijarnos que nuestros documentos se irán haciendo más complejos. Para hacer una consulta con campos específicos, se debe hacer de la siguiente manera:

```sh
> db.libros.find({}, { nombre: 1 })
```

En dado caso de que no quisieramos el id lo podríamos consultar de la siguiente manera:

```sh
> db.libros.find({}, { nombre: 1, _id: 0 })
```

__¿Cómo obtener todos los libros cuya publicación sea mayor a 2000?__

Para ello debemos apoyarnos de los [operadores lógicos](https://docs.mongodb.com/manual/reference/operator/query/) que nos ofrece MongoDB. Algunos son:

```sh
$gt     >   greater than
$gte    >=  greater than equals
$lt     <   less than
$lte    <=  less than equals
```

Entonces podemos ejecutar la siguiente consulta:

```sh
> db.libros.find({ primera_edicion: { $gt: 2000 } }, { nombre: 1 })
```

Podemos ver que nos trae _Triple representatividad_ y _El código Da Vinci_

Para hacer un between se hace de la siguiente manera:

```sh
> db.libros.find({ primera_edicion: { $gte: 1940, $lte: 2000 } }, { nombre: 1 })
```

Aquí aparecen _El señor de los anillos_, _El principito_ y _El nombre de la rosa_. Y si queremos complicarlo más podemos hacer lo siguiente:

```sh
> db.libros.find({ primera_edicion: { $gte: 1940, $lte: 2000 }, numero: { $gt: 100 } }, { nombre: 1 })
```

El resultado es _El señor de los anillos_ y _El principito_.

## <a name="lesson11"></a> Lección #11 - Cursores
- - -
Un curso es la forma en la cual podemos acceder a un documento, poder leerlo, poder modificarlo y poder trabajar con él. En lecciones anteriores, el método find() nos ha devuelto un cursor, pero no hemos trabajado con ellos porque solo lo hemos usado para lectura. Hagamos un ejemplo:

```sh
> use codigo_facilito;
> for(i = 0; i < 100; i++) { db.test.insert({ valor: i }) }
```

Si hay más de 20 registros en MongoDB, este nos mostrará los valores de 20 en 20. Si escribimos it y le damos enter nos seguirá mostrando más registros hasta que terminemos.

Para poder trabajar con cursores podemos hacerlo de la siguiente manera:

```sh
> var cursor = db.test.find()
```

Ahora si escribimos cursor y le damos enter, podemos ver cumple con la misma función de la declaración anterior. La cuestión es que un cursor solo puede ser usado una sola vez.

También podemos hacer cosas de este estilo:

```sh
> var cursor = db.test.find({ valor: { $gt: 98 } })
```

### Propósito de un cursor

El cursor al ser un objeto tiene métodos, por lo tanto podemos hacer cosas de este estilo:

```sh
> var cursor = db.test.find({ valor: { $gt: 98 } })
> cursor.forEach( function(d) { print(d) } )
[object BSON]
```

Un __Objeto BSON__ es el binario de un objeto JSON. Pero también podemos iterar por cada uno de los valores del objeto del cursor. Por ejemplo:

```sh
> var cursor = db.test.find()
> cursor.forEach( function(d) { print(d.valor) } )
```

Puede servir para lectura... Pero también puede servir para escritura. Por ejemplo:

```sh
> var cursor = db.test.find()
> cursor.forEach( function(d) { d.valor = 100; db.test.save(d); } )
```

Y si ejecutamos:

```sh
> var cursor = db.test.find()
> cursor.forEach( function(d) { print(d.valor) } )
```

Podemos observar que todos los valores fueron modificados a 100. También como hemos visto en el curso podemos modificar, quitar o agregar más propiedades a nuestros documentos.

## <a name="lesson12"></a> Lección #12 - Métodos de Cursores
- - -
Para esta lección, trabajaremos con la siguiente colección:

```sh
> use codigo_facilito;
> uno = { nombre: "McIntosh", valor: 12, venta: true }
> dos = { nombre: "Empire", valor: 13, venta: true }
> tres = { nombre: "Red Rome", valor: 15, venta: true }
> cuatro = { nombre: "Reineta", valor: 10, venta: false }
> cinco = { nombre: "Fuji", valor: 10, venta: true }
> seis = { nombre: "Royal Gala", valor: 50, venta: true }
> siete = { nombre: "Delicious", valor: 2, venta: false }
> db.manzanas.insert([uno, dos, tres, cuatro, cinco, seis, siete])
```

Podemos saber la cantidad de registros que tiene la colección con el comando:

```sh
> db.manzanas.find().count()
7
```

__¿Cómo podemos obtener las manzanas ordenas de menor a mayor?__

Para ello ejecutamos el comando:

```sh
> db.manzanas.find().sort( { valor: 1 } )
{ "_id" : ObjectId("5e90c916f92939a384313e05"), "nombre" : "Delicious", "valor" : 2, "venta" : false }
{ "_id" : ObjectId("5e90c916f92939a384313e02"), "nombre" : "Reineta", "valor" : 10, "venta" : false }
{ "_id" : ObjectId("5e90c916f92939a384313e03"), "nombre" : "Fuji", "valor" : 10, "venta" : true }
{ "_id" : ObjectId("5e90c916f92939a384313dff"), "nombre" : "McIntosh", "valor" : 12, "venta" : true }
{ "_id" : ObjectId("5e90c916f92939a384313e00"), "nombre" : "Empire", "valor" : 13, "venta" : true }
{ "_id" : ObjectId("5e90c916f92939a384313e01"), "nombre" : "Red Rome", "valor" : 15, "venta" : true }
{ "_id" : ObjectId("5e90c916f92939a384313e04"), "nombre" : "Royal Gala", "valor" : 50, "venta" : true }
```

De esta forma lo obtenemos de forma ascendente. Si queremos el caso contrario, ejecutamos:

```sh
> db.manzanas.find().sort( { valor: -1 } )
{ "_id" : ObjectId("5e90c916f92939a384313e04"), "nombre" : "Royal Gala", "valor" : 50, "venta" : true }
{ "_id" : ObjectId("5e90c916f92939a384313e01"), "nombre" : "Red Rome", "valor" : 15, "venta" : true }
{ "_id" : ObjectId("5e90c916f92939a384313e00"), "nombre" : "Empire", "valor" : 13, "venta" : true }
{ "_id" : ObjectId("5e90c916f92939a384313dff"), "nombre" : "McIntosh", "valor" : 12, "venta" : true }
{ "_id" : ObjectId("5e90c916f92939a384313e02"), "nombre" : "Reineta", "valor" : 10, "venta" : false }
{ "_id" : ObjectId("5e90c916f92939a384313e03"), "nombre" : "Fuji", "valor" : 10, "venta" : true }
{ "_id" : ObjectId("5e90c916f92939a384313e05"), "nombre" : "Delicious", "valor" : 2, "venta" : false }
```

También tenemos un método __limit()__ que nos sirve para limitar la cantidad de registros que se trae de la consulta. Ejemplo:

```sh
> db.manzanas.find().sort( { valor: -1 } ).limit(3)
{ "_id" : ObjectId("5e90c916f92939a384313e04"), "nombre" : "Royal Gala", "valor" : 50, "venta" : true }
{ "_id" : ObjectId("5e90c916f92939a384313e01"), "nombre" : "Red Rome", "valor" : 15, "venta" : true }
{ "_id" : ObjectId("5e90c916f92939a384313e00"), "nombre" : "Empire", "valor" : 13, "venta" : true }
```

Vamos a complicar un poco más esto... Supongamos que queremos __obtener el segundo tipo de manzana más caro__. Para ello podemos usar el método __skip(n)__. Ejemplo:

```sh
> db.manzanas.find().sort( { valor: -1 } ).skip(1).limit(1)
{ "_id" : ObjectId("5e90c916f92939a384313e01"), "nombre" : "Red Rome", "valor" : 15, "venta" : true }
```

__IMPORTANTE:__ El método count() cuenta los registros de la colección, no del skip y tampoco del limit.

Si queremos contar cuantos registros tengo después de aplicarle varios métodos como el skip() y el limit() debemos usar el método __size()__. Ejemplo:

```sh
> db.manzanas.find().sort( { valor: -1 } ).skip(3).size()
4
```

De esta forma, podemos delegar a la base de datos para que aplique estas condiciones sin necesidad que lo haga la aplicación.

## <a name="lesson13"></a> Lección #13 - Arreglos
- - -
Cuando trabajamos en bases de datos relaciones, existe el contexto de una relación 1xN. En MongoDB podemos cumplir el mismo objetivo con los arreglos. En esta lección vamos a declarar simplemente arreglos. Ejemplo:

```sh
> var arreglo = [1, 2, 3]
> var usuario = { nombre: "test", valores: arreglo }
> db.usuarios.insert(usuario)
>
> db.usuarios.find()
{ "_id" : ObjectId("5e90cd6ef92939a384313e06"), "nombre" : "test", "valores" : [ 1, 2, 3 ] }
```

Si queremos agregar arreglos a un registro existente es de la misma forma como aprendimo en lecciones anteriores. Ejemplo:

```sh
> db.usuarios.update({}, { $set: { nuevo_arreglo: [4, 5, 6] } })
WriteResult({ "nMatched" : 1, "nUpserted" : 0, "nModified" : 1 })
> db.usuarios.find()
{ "_id" : ObjectId("5e90cd6ef92939a384313e06"), "nombre" : "test", "valores" : [ 1, 2, 3 ], "nuevo_arreglo" : [ 4, 5, 6 ] }
```

Vamos a agregar nuevos valores a un arreglo existente, para ello debemos usar la variable __\$addToSet__ para lograr el cometido en vez de la variable \$set. Ejemplo:

```sh
> db.usuarios.update({}, { $addToSet: { valores: 4 } })
WriteResult({ "nMatched" : 1, "nUpserted" : 0, "nModified" : 1 })
> db.usuarios.find()
{ "_id" : ObjectId("5e90cd6ef92939a384313e06"), "nombre" : "test", "valores" : [ 1, 2, 3, 4 ], "nuevo_arreglo" : [ 4, 5, 6 ] }
```

Si queremos usar de nuevo:

```sh
> db.usuarios.update({}, { $addToSet: { valores: 4 } })
WriteResult({ "nMatched" : 1, "nUpserted" : 0, "nModified" : 0 })
```

Veremos que nada ha sido modificado, esto ocurre porque la variable \$addToSet busca primero si el atributo existe dentro del arreglo. Si existe, no hace nada. Caso contrario, lo agrega. Si lo que queremos es que se repita el valor, debemos usar la variable reservada __\$push__. Ejemplo:

```sh
> db.usuarios.update({}, { $push: { valores: 4 } })
WriteResult({ "nMatched" : 1, "nUpserted" : 0, "nModified" : 1 })
>
> db.usuarios.find()
{ "_id" : ObjectId("5e90cd6ef92939a384313e06"), "nombre" : "test", "valores" : [ 1, 2, 3, 4, 4 ], "nuevo_arreglo" : [ 4, 5, 6 ] }
```

En este caso, el número 4 si fué agregado. Esto está muy bien si queremos agregar elementos 1 por 1... Pero, ¿Qué pasa cuando tenemos un arreglo y queremos agregarlo a nuestro elemento? Para ello debemos hacerlo con la variable reservada __\$each__. Ejemplo:

```sh
> db.usuarios.update({}, { $push: { valores: { $each: [6, 7] } } })
WriteResult({ "nMatched" : 1, "nUpserted" : 0, "nModified" : 1 })

> db.usuarios.find()
{ "_id" : ObjectId("5e90cd6ef92939a384313e06"), "nombre" : "test", "valores" : [ 1, 2, 3, 4, 4, 6, 7 ], "nuevo_arreglo" : [ 4, 5, 6 ] }
```

_Nota: También podemos usar el each para \$addToSet_

**¿Cómo hacemos para agregar elementos en una posición específica?**

Para ello tenemos la variable reservada **\$position**, ejemplo:

```sh
> db.usuarios.update({}, { $push: { valores: { $each: [100, 101], $position: 4 } } })
WriteResult({ "nMatched" : 1, "nUpserted" : 0, "nModified" : 1 })

> db.usuarios.find()
{ "_id" : ObjectId("5e90cd6ef92939a384313e06"), "nombre" : "test", "valores" : [ 1, 2, 3, 4, 4, 6, 7 ], "nuevo_arreglo" : [ 4, 5, 6 ] }
```

_\$position no funciona con \$addToSet_

**¿Cómo hacemos para agregar elementos de forma ordenada?**

Para ello, debemos usar la variable reservada **\$sort** de la siguiente manera:

```sh
> db.usuarios.update({}, { $push: { valores: { $each: [95, 97], $sort: 1 } } })
WriteResult({ "nMatched" : 1, "nUpserted" : 0, "nModified" : 1 })

> db.usuarios.find()
{ "_id" : ObjectId("5e90cd6ef92939a384313e06"), "nombre" : "test", "valores" : [ 1, 1, 2, 2, 3, 4, 4, 6, 7, 95, 97, 100, 101 ], "nuevo_arreglo" : [ 4, 5, 6 ] }
```

Pero no solo lo agrega de forma ordenada... Sino que realiza todo el ordenamiento.

Si lo deseamos ordenar de forma ascendente o descendente nuestro arreglo, debemos ejecutar lo siguiente:

```sh
> db.usuarios.update({}, { $push: { valores: { $each: [], $sort: -1 } } })
WriteResult({ "nMatched" : 1, "nUpserted" : 0, "nModified" : 1 })

> db.usuarios.find()
{ "_id" : ObjectId("5e90cd6ef92939a384313e06"), "nombre" : "test", "valores" : [ 101, 100, 97, 95, 7, 6, 4, 4, 3, 2, 2, 1, 1 ], "nuevo_arreglo" : [ 4, 5, 6 ] }
```

## <a name="lesson14"></a> Lección #14 - Delete arreglos
- - - 
Para eliminar elementos del arreglo debemos hacer uso de la palabra reservada **\$pull**, ejemplo:

```sh
> db.usuarios.update({}, { $pull: { valores: 1 } })
WriteResult({ "nMatched" : 1, "nUpserted" : 0, "nModified" : 1 })

> db.usuarios.find()
{ "_id" : ObjectId("5e90cd6ef92939a384313e06"), "nombre" : "test", "valores" : [ 101, 100, 97, 95, 7, 6, 4, 4, 3, 2, 2 ], "nuevo_arreglo" : [ 4, 5, 6 ] }
```

_Nota: Si hay valores duplicados en el arreglo, también serán eliminados._

También podemos complicar los delete, en este caso borraremos todos los valores mayores a 50:

```sh
> db.usuarios.update({}, { $pull: { valores: { $gte: 50 } } })
WriteResult({ "nMatched" : 1, "nUpserted" : 0, "nModified" : 1 })

> db.usuarios.find()
{ "_id" : ObjectId("5e90cd6ef92939a384313e06"), "nombre" : "test", "valores" : [ 7, 6, 4, 4, 3, 2, 2 ], "nuevo_arreglo" : [ 4, 5, 6 ] }
```

Para borrar varios elementos dentro del arreglo debemos usar la variable reservada **\$pullAll**. Ejemplo:

```sh
> db.usuarios.update({}, { $pullAll: { valores: [2, 4] } })
WriteResult({ "nMatched" : 1, "nUpserted" : 0, "nModified" : 1 })

> db.usuarios.find()
{ "_id" : ObjectId("5e90cd6ef92939a384313e06"), "nombre" : "test", "valores" : [ 7, 6, 3 ], "nuevo_arreglo" : [ 4, 5, 6 ] }
```

## <a name="lesson15"></a> Lección #15 - Select arreglos
- - - 
Para esta lección, vaciaremos nuestra colección de usuarios y crearemos un registro de la siguiente manera:

```sh
> use codigo_facilito;
> db.usuarios.remove({})
>
> db.usuarios.insert({ nombre: "Eduardo", ejemplo: ["MongoDB", "C#", "SQL", "Java", "Python"] })
```

Si queremos ver solo el arreglo, lo hacemos de la siguiente manera:

```sh
> db.usuarios.find({}, { _id: 0, ejemplo: 1 })
```

Nosotros podemos seleccionar una muestra del arreglo con la variable reservada **\$slice**. Ejemplo:

```sh
> db.usuarios.find({}, { _id: 0, ejemplo: { $slice: 3 } })
{ "nombre" : "Eduardo", "ejemplo" : [ "MongoDB", "C#", "SQL" ] }
```

Si queremos el último elemento, usamos valores negativos. Ejemplo:

```sh
> db.usuarios.find({}, { _id: 0, ejemplo: { $slice: -1 } })
{ "nombre" : "Eduardo", "ejemplo" : [ "Python" ] }
```

También podemos agarrar una parte del arreglo indicandole la posición de la siguiente manera:

```sh
> db.usuarios.find({}, { _id: 0, ejemplo: { $slice: [1, 3] } })
{ "nombre" : "Eduardo", "ejemplo" : [ "C#", "SQL", "Java" ] }
```

Pero fíjense que me está apareciendo el nombre y por ello debemos desactivarlo:

```sh
> db.usuarios.find({}, { _id: 0, nombre: 0, ejemplo: { $slice: [1, 3] } })
{ "ejemplo" : [ "C#", "SQL", "Java" ] }
```

Para escribir parámetros de búsquedas en los arreglos podemos hacerlo con la variable reserveda **\$in** de la siguiente manera:

```sh
> db.usuarios.find({ ejemplo: { $in: ["MongoDB"] } }, { _id: 0, nombre: 0, ejemplo: { $slice: [1, 3] } })
{ "ejemplo" : [ "C#", "SQL", "Java" ] }
```

_Nota: Las búsquedas de los strings como en cualquier base de datos es case sensitive._

También podemos hallar resultados incluso cuando un elemento del arreglo no se encuentre. Ejemplo:

```sh
> db.usuarios.find({ ejemplo: { $in: ["XML", "MongoDB"] } }, { _id: 0, nombre: 0, ejemplo: { $slice: [1, 3] } })
{ "ejemplo" : [ "C#", "SQL", "Java" ] }
```

Si queremos hacer una búsqueda "Not in" podemos usar la variable reservada **\$nin**. Ejemplo:

```sh
> db.usuarios.find({ ejemplo: { $nin: ["XML", "MongoDB"] } }, { _id: 0, nombre: 0, ejemplo: { $slice: [1, 3] } })
```

Crearemos otro documento para hacer un ejemplo:

```sh
> use codigo_facilito;
> db.usuarios.insert({ nombre: "Dos", ejemplo: ["XML", "Go"] });
```

Si ejecutamos nuevamente:

```sh
> db.usuarios.find({ ejemplo: { $nin: ["XML", "MongoDB"] } }, { _id: 0, ejemplo: 1 })
```

No conseguirá ninguno de los dos registros.

## <a name="lesson16"></a> Lección #16 - Group
- - - 
Para esta lección, trabajaremos con la siguiente colección:

```sh
> use codigo_facilito;
> uno = { item: "caja", valor: 100, existencia: 20 }
> dos = { item: "caja", valor: 50, existencia: 25 }
> tres = { item: "latas", valor: 450, existencia: 20 }
> cuatro = { item: "botellas", valor: 50, existencia: 150 }
> db.items.insert([uno, dos, tres, cuatro])
```

La idea es agrupar los elementos donde se repitan los valores. Por ejemplo:

```sh
> db.items.aggregate([{ $group: { _id: "$item" } } ])
{ "_id" : "latas" }
{ "_id" : "caja" }
{ "_id" : "botellas" }
```

Es muy fácil perderse con esa sintáxis pero funciona de la siguiente manera. El método aggregate() recibe como parámetro un arreglo. Dentro de ese arreglo, pasaremos un documento con la variable reservada **\$group** que sirve para agrupar y dentro vamos a colocar el nombre de nueva propiedad generada y lo que va entre comillas dobles es el nombre del campo acompañado del signo dolar y entre comillas dobles.

Una forma de conocer los repetidos es de la siguiente manera:

```sh
> db.items.aggregate([{ $group: { _id: "$item", "repetidos": { $sum: 1 } } } ])
{ "_id" : "latas", "repetidos" : 1 }
{ "_id" : "caja", "repetidos" : 2 }
{ "_id" : "botellas", "repetidos" : 1 }
```

_Con_ **\$sum** _se suman los repetidos_

Si colocamos un 2 en \$sum este sumará de dos en dos:

```sh
> db.items.aggregate([{ $group: { _id: "$item", "repetidos": { $sum: 2 } } } ])
{ "_id" : "latas", "repetidos" : 2 }
{ "_id" : "caja", "repetidos" : 4 }
{ "_id" : "botellas", "repetidos" : 2 }
```

_Es solo un dato curioso._

También podemos complicarlo más de la siguiente manera:

```sh
> db.items.aggregate([{ $group: { _id: "$item", "repetidos": { $sum: 1 }, "suma_valor": { $sum: "$valor" } } } ])
{ "_id" : "caja", "repetidos" : 2, "suma_valor" : 150 }
{ "_id" : "botellas", "repetidos" : 1, "suma_valor" : 50 }
{ "_id" : "latas", "repetidos" : 1, "suma_valor" : 450 }
```

Si queremos sacar el promedio podemos hacerlo de la siguiente manera:

```sh
> db.items.aggregate([{ $group: { _id: "$item", "promedio": { $avg: "$valor" } } } ])
{ "_id" : "caja", "promedio" : 75 }
{ "_id" : "botellas", "promedio" : 50 }
{ "_id" : "latas", "promedio" : 450 }
```

## <a name="lesson17"></a> Lección #17 - Expresiones Regulares
- - - 
Para esta lección, trabajaremos con la siguiente colección:

```sh
> use codigo_facilito;
> uno = { correo: "test_test8@gmail.com" }
> dos = { correo: "test_120@gmail.com" }
> tres = { correo: "test_120@hotmail.mx" }
> db.correos.insert([uno, dos, tres])
```

En bases de datos relacionales podemos hacer uso del LIKE %. En MongoDB tenemos un comportamiento similar y es de la siguiente manera:

```sh
> db.correos.find({ correo: /@/ })
{ "_id" : ObjectId("5e90f68dc30c31f1d8fbebfe"), "correo" : "test_test8@gmail.com" }
{ "_id" : ObjectId("5e90f68dc30c31f1d8fbebff"), "correo" : "test_120@gmail.com" }
{ "_id" : ObjectId("5e90f68dc30c31f1d8fbec00"), "correo" : "test_120@hotmail.mx" }
```

Si queremos los valores que terminen con .com:

```sh
> db.correos.find({ correo: /.com$/ })
{ "_id" : ObjectId("5e90f68dc30c31f1d8fbebfe"), "correo" : "test_test8@gmail.com" }
{ "_id" : ObjectId("5e90f68dc30c31f1d8fbebff"), "correo" : "test_120@gmail.com" }
```

El signo \$ significa que debe ser estrictamente esa cadena de caracter. Ejemplo:

```sh
> db.correos.find({ correo: /il.co$/ })
```

No arroja ningún resultado. Pero si colocamos:

```sh
> db.correos.find({ correo: /il.co/ })
{ "_id" : ObjectId("5e90f68dc30c31f1d8fbebfe"), "correo" : "test_test8@gmail.com" }
{ "_id" : ObjectId("5e90f68dc30c31f1d8fbebff"), "correo" : "test_120@gmail.com" }
```

Asi si aparecen los resultados.

Si queremos todos los correos que empiecen por test:

```sh
> db.correos.find({ correo: /^test/ })
{ "_id" : ObjectId("5e90f68dc30c31f1d8fbebfe"), "correo" : "test_test8@gmail.com" }
{ "_id" : ObjectId("5e90f68dc30c31f1d8fbebff"), "correo" : "test_120@gmail.com" }
{ "_id" : ObjectId("5e90f68dc30c31f1d8fbec00"), "correo" : "test_120@hotmail.mx" }
```

Podemos ser más específicos de la siguiente forma:

```sh
> db.correos.find({ correo: /^test_120@g/ })
{ "_id" : ObjectId("5e90f68dc30c31f1d8fbebff"), "correo" : "test_120@gmail.com" }
```

## <a name="lesson18"></a> Lección #18 - Documentos embebidos
- - - 
Los documentos embebidos son una parte fundamental de trabajar con NoSQL porque vienen a reemplazar ciertos conocimientos que tenemos en bases de datos relacionales. En bases de datos relacionales tenemos el concepto de una relación 1x1 (que consiste en abstraer cierta información de una tabla de la base de datos). En una base de datos relacional podemos separarlo de diversas formas:

1. En dos tablas y eso nos da mayor facilidad para interpretar la información, y hacer consultas más complejas. ¿Qué problemas tiene el separar los documentos? Que debemos colocar el identificador para poder relacionar una tabla con la otra (Puede ser en cualquiera de las dos tablas).
2. No separarlo y trabajar con la misma tabla. Esto depende de lo que estemos desarrollando en el proyecto, veremos si es viable o no separar las tablas (o colecciones).

En MongoDB podemos embeber documentos que no es más que poner un documento dentro de otro. Por ejemplo:

```sh
alumno
{
    "nombre": "Alumno",
    "Apellido": "Apellido alumno",
    "edad": 27,
    "matricula":
    {
        "matricula": "xxxxx",
        "año": 2016,
        "folio": 123456789,
        "nueva": true
    }
}
```

_Nota: El tamaño máximo de un documento en MongoDB puede ser de 16MB, eso es muchísimo._

Para embeber un documento es sencillo, podemos hacerlo de la siguiente manera:

```sh
> alumno = { nombre: "Usuario", apellido: "Apellido", edad: 27, sexo: "Masculino" }
> matricula = { numero: 12345, año: 2020, renovada: false }
> alumno.matricula = matricula
> alumno
```

La otra forma es la siguiente:

```sh
> alumno_dos = { nombre: "Usuario", apellido: "Apellido", edad: 27, sexo: "Masculino", matricula: { numero: 12345, año: 2020, renovada: false } }
> alumno_dos
```

Para almacenarlo:

```sh
> use codigo_facilito;
> db.alumnos.insert([alumno, alumno_dos]);
```

## <a name="lesson19"></a> Lección #19 - CRUD Documentos embebidos
- - - 
Para hacer consultas de un documento embebido debemos hacerlo de la siguiente manera:

```sh
> db.alumnos.find({ "matricula.numero": 12345 })
```

También podemos obtener ciertos atributos. Ejemplo:

```sh
> db.alumnos.find({ "matricula.numero": 12345 }, { "matricula": 1 })
{ "_id" : ObjectId("5e910c9bc30c31f1d8fbec01"), "matricula" : { "numero" : 12345, "año" : 2020, "renovada" : false } }
```

O solo ciertos atributos del documento embebido:

```sh
> db.alumnos.find({ "matricula.numero": 12345 }, { "matricula.año": 1 })
{ "_id" : ObjectId("5e910c9bc30c31f1d8fbec01"), "matricula" : { "año" : 2020 } }
```

De ahora en adelante, se recomienda que para todos nuestros queries usemos las comillas dobles para nombres a la propiedad (o campo) donde deseemos buscar.

Actualicemos las matrículas de forma masiva de la siguiente manera:

```sh
> db.alumnos.update({}, { $set: { "matricula.año": 2021 } }, { multi: true })
```

Para eliminar, lo podemos hacer de la siguiente manera:

```sh
> db.alumnos.remove({ "matricula.renovada": false })
WriteResult({ "nRemoved" : 2 })
```

Ahora crearemos un documento embebido de manera rápida y sencilla:

```sh
> alumno = { nombre: "test" }
> db.alumnos.insert(alumno)
> matricula = { folio: 123 }
> db.alumnos.update({}, { $set: { matricula: matricula}})
```

## <a name="lesson20"></a> Lección #20 - Relación uno a muchos
- - - 
Existen diversas formas para trabajar con relaciones uno a muchos como hemos visto a lo largo del curso como separar las colecciones, embeber los documentos y asociar un objectId para hacer referencia con la otra colección. Sin embargo, en las futuras lecciones trabajaremos con los arreglos para solventar esta problemática. Supongamos que tengamos al siguiente documento (Un trabajador que puede tener múltiples direcciones):

```sh
> trabajador = { nombre: "Trabajador", edad: 27 }
> 
> uno = { direccion: "dirección uno" }
> dos = { direccion: "dirección dos" }
> tres = { direccion: "dirección tres" }
> trabajador.direcciones = [uno, dos, tres]
```

Esto se ve de la siguiente manera:

```sh
> trabajador
{
        "nombre" : "Trabajador",
        "edad" : 27,
        "direcciones" : [
                {
                        "direccion" : "dirección uno"
                },
                {
                        "direccion" : "dirección dos"
                },
                {
                        "direccion" : "dirección tres"
                }
        ]
}
```

## <a name="lesson21"></a> Lección #21 - ElemMatch
- - - 
Para esta lección, trabajaremos con la siguiente colección:

```sh
> use codigo_facilito;
> trabajador = { nombre: "Juan", apellido: "Pérez", edad: 30, sexo: "Masculino", tipo_sangre: "O", cedula: { cedula: 987654321, año_registro: 2016, año_expiracion: 2018 }, direcciones: [ { calle: "calle número uno", número: 12, piso: 4, colonia: "Colonia uno", uso: "entrega de paquetes" }, { calle: "calle número dos", número: 11, colonia: "Colonia dos", uso: "Contacto personal" } ], estados: [ "Casado", "Saludable", "Sin deudas" ] }
> db.trabajadores.insert(trabajador)
```

Cuando ejecutamos el comando db.trabajadores.find() tenemos el problema de que el documento te lo muestra de forma lineal, lo cuál lo hace problemático de leer. Para ello, existe un método llamado pretty() que nos ayuda a leer mejor nuestro objeto JSON.

```sh
> db.trabajadores.find().pretty()
```

Y como hemos visto de lecciones anteriores, podemos buscar nuestro documento de distintas formas:

```sh
> db.trabajadores.find({ nombre: "Juan" })
> db.trabajadores.find({ "cedula.año_registro": 2016 })
```

Pero __¿Cómo buscamos filtrando por arreglos (relación 1xN)?__ Se hace de la siguiente manera:

```sh
> db.trabajadores.find({ "direcciones.uso": "Contacto personal" })
```

Como podemos ver, me retornará cualquier registro que contega por lo menos como **uso de la dirección** la que hemos indicado. Hagamos el mismo ejemplo pero con piso:

```sh
> db.trabajadores.find({ "direcciones.piso": 4 })
{ "_id" : ObjectId("5e916fe2c30c31f1d8fbec04"), "nombre" : "Juan", "apellido" : "Pérez", "edad" : 30, "sexo" : "Masculino", "tipo_sangre" : "O", "cedula" : { "cedula" : 987654321, "año_registro" : 2016, "año_expiracion" : 2018 }, "direcciones" : [ { "calle" : "calle número uno", "número" : 12, "piso" : 4, "colonia" : "Colonia uno", "uso" : "entrega de paquetes" }, { "calle" : "calle número dos", "número" : 11, "colonia" : "Colonia dos", "uso" : "Contacto personal" } ], "estados" : [ "Casado", "Saludable", "Sin deudas" ] }
```

Aunque uno de los recursos no tenga dicha propiedad, él seguirá buscando en los otros recursos.

También podemos buscar mediante dos valores:

```sh
> db.trabajadores.find({ "direcciones.uso": "Contacto personal", "direcciones.colonia": "Colonia dos" })
```

Pero existe una forma más elegante de realizarlo y es con la variable reservada **\$elemMatch**:

```sh
> db.trabajadores.find({ direcciones: { $elemMatch: { uso: "Contacto personal" } } })
```

Cualquiera de las dos formas son correctas. Sin embargo se recomienda el uso de la primera **siempre y cuando** filtremos por solo uno atributo, del resto es mejor usar el \$elemMatch.

Si estamos filtrando por atributos podemos hacerlo como en lecciones anteriores:

```sh
> db.trabajadores.find({ direcciones: { $elemMatch: { uso: "Contacto personal" } } }, { _id: false, direcciones: true })
```

Pero si queremos los atributos del \$elemMatch lo hacemos de la siguiente manera:

```sh
> db.trabajadores.find({ direcciones: { $elemMatch: { uso: "Contacto personal", número: 11 } } }, { _id: false, direcciones: { $elemMatch: { uso: "Contacto personal", número: 11 } } })
{ "direcciones" : [ { "calle" : "calle número dos", "número" : 11, "colonia" : "Colonia dos", "uso" : "Contacto personal" } ] }
```

## <a name="lesson22"></a> Lección #22 - Update SubDocumentos
- - - 
Las cosas se complican acá porque debemos iterar es sobre un arreglo. Lo ideal sería que buscasemos en un valor como tal. Por ejemplo:

```sh
> db.trabajadores.find( { "direcciones.número": 12 }, { _id: false, direcciones: { $elemMatch: { número: 12 } } } )
```

Hay otra forma de mostrarlo y es de la siguiente manera:

```sh
> db.trabajadores.find( { "direcciones.número": 12 }, { _id: false, "direcciones.$": true } )
```

**IMPORTANTE: El filtro de la izquierda sirve para buscar las direcciones donde el número sea el 12... Pero, si tenemos un \$elemMatch a la derecha este servirá para filtrar el subDocumento, por ejemplo: donde el uso sea "entrega de paquetes"**. Ejemplo:

```sh
> db.trabajadores.find( { "direcciones.número": { $gt: 10 } }, { _id: false, direcciones: { $elemMatch: { uso: "entrega de paquetes" } } } )
```

En los updates, El \$ es la forma más eficiente como lo vamos a lograr. Ejemplo:

```sh
> db.trabajadores.update( { "direcciones.número": 12 }, { $set: { "direcciones.$": { número: 999 } } } )
```

_Aquí no modificamos solo el número sino todo el documento que tenía el número 12_

En el caso de que hubiesen valores duplicados con el mismo número, MongoDB agarrará por defecto el primero. Así que debemos ser muy cuidadosos y usar un _id.

Si queremos sobreescribir solo el número lo hacemos de la siguiente forma:

```sh
> db.trabajadores.update( { "direcciones.número": 11 }, { $set: { "direcciones.$.uso": "cambio de uso" } } )
```

Podemos comprobarlo con el comando:

```sh
> db.trabajadores.find().pretty()
```

## <a name="lesson23"></a> Lección #23 - ObjectId
- - - 
Es un identificador único que te crea MongoDB por defecto. Sin embargo, nosotros podemos establecer nuestro propio id de la siguiente manera:

```sh
> var documento = { _id: "Nuevo Id", name: "otro" }
> db.id_pruebas.insert(documento)
>
> db.id_pruebas.find()
{ "_id" : "Nuevo Id", "name" : "otro" }
```

Veremos que el _id ahora es que el que establecimos nosotros. Otro ejemplo:

```sh
> var documento = { _id: 4, name: "otro" }
> db.id_pruebas.insert(documento)
>
> db.id_pruebas.find()
{ "_id" : "Nuevo Id", "name" : "otro" }
{ "_id" : 4, "name" : "otro" }
```

Todo sigue siendo de la misma forma. Si queremos crear nuestro ObjectId de forma manual, lo hacemos de la siguiente manera:

```sh
> new ObjectId()
ObjectId("5e91d2f3c30c31f1d8fbec05")
```

Esto genera un string del tipo Object de 24 caracteres y funciona de esta manera:

5e91d2f3c30c31f1d8fbec05 #12 bytes = 24 caracteres.
* 0-3 bytes = timestamp.
* 4-6 bytes = identificador único de nuestra máquina.
* 7-8 bytes = PID.
* 9-11 bytes = número incremental.

Por esto este _id **jamás** se va a repetir en ninguna colección y tampoco en ningún servidor (a diferencia de estar usando un string, un autoincremental, etc.).

Los ObjectIds también ya están indexados y vienen ordenados de menor a mayor gracias a su número incremental y por el timestamp del principio.

Por eso se recomienda a toda costa usar los ObjectIds.

## <a name="lesson24"></a> Lección #24 - Create Collection
- - - 
A lo largo del curso hemos visto como MongoDB nos ha otorgado la flexibilidad a la hora de crear documentos. Sin embargo, esto puede ser un problema sino tenemos un estándar claro y un orden a la hora de almacenar documentos. Por ejemplo, una persona puede almacenar en un documentos cosas de este estilo:

```sh
> { nombre: "Pepe" }
> { Nombre: "Pepe" }
>
> { sexo: "Masculino" }
> { sexo: "M" }
```

Y n errores más pueden ocurrir.

Vamos a usar el método **createCollection()** la cuál puede recibir varios parámetros que nos permiten limitar la cantidad de campos o propiedades que puede tener, la cantidad de registros, etc. Para más información podemos consultar en el siguiente [link](https://docs.mongodb.com/manual/reference/method/db.createCollection/). Crearemos una colección de la siguiente manera:

```sh
> db.createCollection("Prueba_C",
> {
>   validator: { $and:
>     [
>         { nombre: { $type: "string" } },
>         { sexo: { $in: ["M", "F"] } }
>     ]
>   }
> })
{ "ok" : 1 }
```

Podemos comprobar si se creó con show collections. Hagamos una prueba:

```sh
> var documento = { nombre: 12, sexo: "M" }
> db.Prueba_C.insert(documento)
WriteResult({
        "nInserted" : 0,
        "writeError" : {
                "code" : 121,
                "errmsg" : "Document failed validation"
        }
})
```

Podemos fijarnos que existe un error porque el nombre no es correcto. Si lo hacemos de forma correcta:

```sh
> var documento = { nombre: "Eduardo", sexo: "M" }
> db.Prueba_C.insert(documento)
> WriteResult({ "nInserted" : 1 })
```

Hagamos más validaciones. Vamos a colocar un campo email de la siguiente manera:

```sh
> db.createCollection("Prueba_D",
> {
>   validator: { $and:
>     [
>         { nombre: { $type: "string" } },
>         { sexo: { $in: ["M", "F"] } },
>         { email: { $regex: /@/ } }
>     ]
>   }
> })
{ "ok" : 1 }
```

Y hagamos otra prueba:

```sh
> var documento = { nombre: "Eduardo", sexo: "M", email: "eduardo.com" }
> db.Prueba_D.insert(documento)
WriteResult({
        "nInserted" : 0,
        "writeError" : {
                "code" : 121,
                "errmsg" : "Document failed validation"
        }
})
```

Entonces la validación está funcionando. Corrijamos el campo ahora:

```sh
> var documento = { nombre: "Eduardo", sexo: "M", email: "eduardo20.3263@gmail.com" }
> db.Prueba_D.insert(documento)
WriteResult({ "nInserted" : 1 })
```

Hagamos otra validación más:

```sh
> db.createCollection("Prueba_E",
> {
>   validator: { $and:
>     [
>         { nombre: { $type: "string" } },
>         { sexo: { $in: ["M", "F"] } },
>         { email: { $regex: /@/ } },
>         { telefono: { $exists: false } }
>     ]
>   }
> })
{ "ok" : 1 }
```

En este caso estamos diciendo que el campo teléfono no puede existir. Hagamos una prueba:

```sh
> var documento = { nombre: "Eduardo", sexo: "M", email: "eduardo20.3263@gmail.com", telefono: "+58 414 1234567" }
> db.Prueba_E.insert(documento)
WriteResult({
        "nInserted" : 0,
        "writeError" : {
                "code" : 121,
                "errmsg" : "Document failed validation"
        }
})
```

También podemos hacer validaciones para documentos embebidos. Por ejemplo:

```sh
> db.createCollection("Prueba_F",
> {
>   validator: { $and:
>     [
>         { "valor.nombre": { $type: "string" } }
>     ]
>   }
> })
{ "ok" : 1 }
```

Si colocamos un error de la siguiente manera:

```sh
> var documento = { valor: { nombre: 12 } }
> db.Prueba_F.insert(documento)
WriteResult({
        "nInserted" : 0,
        "writeError" : {
                "code" : 121,
                "errmsg" : "Document failed validation"
        }
})
```

## <a name="lesson25"></a> Lección #25 - Índices
- - - 
Para esta lección vamos a crear una colección con 1 millón de documentos de la siguiente manera:

```sh
> use codigo_facilito
> db.usuarios.remove({})
> for (i = 0; i < 1000000; i++) { db.usuarios.insert({ nombre: "usuario" + i, edad: i }) }
```

Un índice en bases de datos es una estructura de datos que mejora la velocidad de las operaciones, por medio de un identificador único de cada fila de una tabla, permitiendo un rápido acceso a los registros de una tabla de una base de datos. Se parece mucho a los índices de un libro.

Si no tiene un índice la base de datos debe buscar registro por registro y demorará muchísimo más la operación.

Hagamos la siguiente búsqueda:

```sh
> db.usuarios.find({ nombre: "usuario0" })
```

Podemos darnos cuenta que la búsqueda tiene un cierto delay, pero, si búscamos por el ObjectId la búsqueda es muchísimo más rápido. Ejemplo:

```sh
> db.usuarios.find({ _id: ObjectId("5e91e82b58d16951cd9b6c4f") })
```

Como vimos en lecciones anteriores, el ObjectId ya está indexado, por eso la búsqueda es muchísimo más rápido. En MongoDB tenemos un método **explain()** que nos sirve para ver todo lo referente de la búsqueda.

```sh
> db.usuarios.find({ _id: ObjectId("5e91e82b58d16951cd9b6c4f") }).explain("executionStats")
```

En el objeto "executionStats" existe una propiedad llamada "executionTimeMillis" donde podemos ver el tiempo que demora la búsqueda. Cuando lo hacemos por _id la búsqueda es entre (0-2)ms aproximadamente pero si lo hacemos por el nombre veremos que puede demorar incluso hasta 392ms. Si queremos ver solo el tiempo que tarda y no todo lo demás lo hacemos con el comando:

```sh
> db.usuarios.find({ _id: ObjectId("5e91e82b58d16951cd9b6c4f") }).explain("executionStats").executionStats.executionTimeMillis
```

Vamos a indexar ahora el campo nombre para agilizar la búsqueda. Es importante saber que cuando indexamos estamos ocupando espacio en la memoria RAM... Por lo tanto, no se aconseja indexar todos los campos, solo debemos hacerlo en aquellos que sean sumamente importantes.

```sh
> db.usuarios.createIndex({ nombre: 1 })
{
        "createdCollectionAutomatically" : false,
        "numIndexesBefore" : 1,
        "numIndexesAfter" : 2,
        "ok" : 1
}
```

_El 1 no significa que sea verdadero sino que vamos a indexar de forma ascendente_

Ahora si buscamos por nombre veremos que el tiempo bajo radicalmente (a 17ms en este ejemplo), pero si lo seguimos consultando puede que llegue a dar incluso 0ms.

## <a name="lesson26"></a> Lección #26 - Respaldo Base Datos (final)
- - - 
Al inicio del curso hicimos uso del comando mongod que sirve para levantar el servidor de MongoDB y el comando mongo que es el cliente para conectarse a MongoDB.

En esta lección vamos a usar dos aplicaciones más que nos sirven para respaldar y restaurar las bases de datos. Para ello debemos ubicarnos en nuestro folder correspondiente y vamos a teclear el comando:

```sh
mongodump --db codigo_facilito
```

Si el proceso fue exitoso, se creará una carpeta dump/codigo_facilito donde tendremos un par por cada colección:
* Un metadata.json
* Un bson

Vamos a eliminar toda la base de datos, para ello entramos a mongo y ejecutamos:

```sh
> use codigo_facilito
> db.dropDatabase
```

Para restaurar la base de datos, debemos salir de mongo y ejecutar el comando:

```sh
mongorestore --db codigo_facilito dump/codigo_facilito
```

Debe indicar un mensaje de este estilo:

```sh
2020-04-11T13:49:20.156-0400    restoring indexes for collection codigo_facilito.usuarios from metadata
2020-04-11T13:49:22.624-0400    finished restoring codigo_facilito.usuarios (1000000 documents, 0 failures)
2020-04-11T13:49:22.624-0400    1000129 document(s) restored successfully. 0 document(s) failed to restore.
```

Y si entramos nuevamente a mongo y ejecutamos show databases, veremos la base de datos restaurada.

Para solo respaldar una colección usamos el siguiente comando:

```sh
mongodump --collection libros --db codigo_facilito
```

Para restaurarlo, ejecutamos el comando:

```sh
mongorestore --collection libros --db codigo_facilito dump/codigo_facilito/libros.bson
```

Debe aparecer un mensaje de este estilo:

```sh
2020-04-11T14:02:21.056-0400    finished restoring codigo_facilito.libros (9 documents, 0 failures)
2020-04-11T14:02:21.056-0400    9 document(s) restored successfully. 0 document(s) failed to restore.
```

Podemos comprobarlo entrando en mongo y ejecutando:

```sh
> use codigo_facilito
> db.libros.find()
```